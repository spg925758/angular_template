import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServiceService } from 'src/app/Services/auth-service.service';
import { LoginService } from 'src/app/Services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor( private formBuilder: FormBuilder,public route:Router,private loginservice :LoginService,private authserv:AuthServiceService) { }
  loginForm: FormGroup|any;
  loading = false;
  submitted = false;
  returnUrl: string|any;



  public loginValid = true;
  public username:any = '';
  public password:any = '';

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }
      this.loading = true;
      this.loginservice.ValidateLogin().subscribe((retn:any)=>{
        if(retn !=null){
          this.authserv.setUserInfo(retn[0]);
          this.route.navigateByUrl("app/dashboard");
        }else{
          this.loading =false;
        }
    },error=>{
      this.loading =false;

    });
  }

  // public onSubmit(): void {
  //   this.loginValid = true;
  //   const loginreq:any={
  //     username: this.username,
  //     password: this.password
  //   }
  //   this.loginservice.ValidateLogin().subscribe((retn:any)=>{
  //     if(retn !=null){
  //       this.authserv.setUserInfo(retn[0]);
  //       this.route.navigateByUrl("app/dashboard");
  //     }else{
  //       this.loginValid=false;
  //     }
  //   });
  // }
}
